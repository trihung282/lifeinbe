// This model was generated by Lumber. However, you remain in control of your models.
// Learn how here: https://docs.forestadmin.com/documentation/v/v6/reference-guide/models/enrich-your-models
module.exports = (sequelize, DataTypes) => {
  const { Sequelize } = sequelize;
  // This section contains the fields of your model, mapped to your table's columns.
  // Learn more here: https://docs.forestadmin.com/documentation/v/v6/reference-guide/models/enrich-your-models#declaring-a-new-field-in-a-model
  const Store = sequelize.define('store', {
    storeId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    lastUpdate: {
      type: DataTypes.DATE,
      defaultValue: "CURRENT_TIMESTAMP",
    },
  }, {
    tableName: 'store',
    underscored: true,
    timestamps: false,
  });

  // This section contains the relationships for this model. See: https://docs.forestadmin.com/documentation/v/v6/reference-guide/relationships#adding-relationships.
  Store.associate = (models) => {
    Store.belongsTo(models.address, {
      foreignKey: {
        name: 'addressIdKey',
        field: 'address_id',
      },
      target: {
        name: 'address_id',
      },
      as: 'address',
    });
    Store.belongsTo(models.staff, {
      foreignKey: {
        name: 'managerStaffIdKey',
        field: 'manager_staff_id',
      },
      target: {
        name: 'staff_id',
      },
      as: 'managerStaff',
    });
    Store.hasMany(models.customer, {
      foreignKey: {
        name: 'storeIdKey',
        field: 'store_id',
      },
      target: {
        name: 'store_id',
      },
      as: 'customers',
    });
    Store.hasMany(models.inventory, {
      foreignKey: {
        name: 'storeIdKey',
        field: 'store_id',
      },
      target: {
        name: 'store_id',
      },
      as: 'inventories',
    });
    Store.hasMany(models.staff, {
      foreignKey: {
        name: 'storeIdKey',
        field: 'store_id',
      },
      target: {
        name: 'store_id',
      },
      as: 'staff',
    });
  };

  return Store;
};
