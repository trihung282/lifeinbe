// This model was generated by Lumber. However, you remain in control of your models.
// Learn how here: https://docs.forestadmin.com/documentation/v/v6/reference-guide/models/enrich-your-models
module.exports = (sequelize, DataTypes) => {
  const { Sequelize } = sequelize;
  // This section contains the fields of your model, mapped to your table's columns.
  // Learn more here: https://docs.forestadmin.com/documentation/v/v6/reference-guide/models/enrich-your-models#declaring-a-new-field-in-a-model
  const Address = sequelize.define('address', {
    addressId: {
      type: DataTypes.INTEGER,
      field: 'address_id',
      primaryKey: true,
    },
    address: {
      type: DataTypes.STRING,
    },
    address2: {
      type: DataTypes.STRING,
    },
    district: {
      type: DataTypes.STRING,
    },
    postalCode: {
      type: DataTypes.STRING,
      field: 'postal_code',
    },
    phone: {
      type: DataTypes.STRING,
    },
    lastUpdate: {
      type: DataTypes.DATE,
      field: 'last_update',
      defaultValue: "CURRENT_TIMESTAMP",
    },
  }, {
    tableName: 'address',
    timestamps: false,
  });

  // This section contains the relationships for this model. See: https://docs.forestadmin.com/documentation/v/v6/reference-guide/relationships#adding-relationships.
  Address.associate = (models) => {
    Address.belongsTo(models.city, {
      foreignKey: {
        name: 'cityIdKey',
        field: 'city_id',
      },
      target: {
        name: 'city_id',
        field: 'city_id',
      },
      as: 'city',
    });
    Address.hasMany(models.customer, {
      foreignKey: {
        name: 'addressIdKey',
        field: 'address_id',
      },
      target: {
        name: 'address_id',
        field: 'address_id',
      },
      as: 'customers',
    });
    Address.hasMany(models.staff, {
      foreignKey: {
        name: 'addressIdKey',
        field: 'address_id',
      },
      target: {
        name: 'address_id',
        field: 'address_id',
      },
      as: 'staff',
    });
    Address.hasMany(models.store, {
      foreignKey: {
        name: 'addressIdKey',
        field: 'address_id',
      },
      target: {
        name: 'address_id',
        field: 'address_id',
      },
      as: 'stores',
    });
  };

  return Address;
};
